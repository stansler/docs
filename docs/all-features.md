---
title: Database Lab feature matrix
sidebar_label: Feature matrix
description: Database Lab by Postgres.ai – the full list of features.
keywords:
  - "Database Lab features"
  - "postgres.ai features"
  - "database lab all features"
---

| Features | Community<br/>Edition | Enterprise<br/>Standard | Enterprise<br/>Premium |
| :------- | :------------------: | :-------------------: | :-------------------: |
| **Features** | **Community<br/>Edition** | **Enterprise<br/>Standard** | **Enterprise<br/>Premium** |
|Price|Free|Pay-as-you-go: <br/>$2.6624 per TiB per hour<br/><br/>Prepaid: starting<br/>at $1,950/month| <a href="mailto:sales@postgres.ai">Contact us</a> |
|||||
|License|AGPLv3|Commercial|Commercial|
|**Platform – all components, with API, CLI, and GUI**|✅|✅|✅|
|**Personal tokens in API, CLI**|❌|✅|✅|
|||||
|**Thin clones (Database Lab)**||||
|Thin cloning in seconds|✅|✅|✅|
|Multiple snapshots and simultaneous independent clones|✅|✅|✅|
|Full API access (REST, CLI, SDK)|✅|✅|✅|
|Snapshot management and recycling|✅|✅|✅|
|Clone management and recycling (of idle clones)|✅|✅|✅|
|Customization of Postgres Docker images|✅|✅|✅|
|Database refresh (from backup system, dumps): periodical or continuous|✅|✅|✅|
|AWS RDS support|✅|✅|✅|
|Anonymization / obfuscation / masking of PII data *(Roadmap)*|✅|✅|✅|
|Simple deployment to AWS or GCP *(Roadmap)*|❌|✅|✅|
|Fast recovery to arbitrary point in time (PITR) *(Roadmap)*|❌|✅|✅|
|||||
|**SQL optimization environment (Joe Bot)**||||
|Session management and recycling|✅|✅|✅|
|Support hypothetical indexes|✅|✅|✅|
|Support hypothetical partitioning *(Roadmap)*|✅|✅|✅|
|Index advisor *(Roadmap)*|✅|✅|✅|
|Slack chatbot|✅|✅|✅|
|Web chatbot|❌|✅|✅|
|Private chats|❌|✅|✅|
|SQL optimization knowledge base|❌|✅|✅|
|SQL query visualization|❌|✅|✅|
|Integration with postgres-checkup, seamless SQL optimization workflow *(Roadmap)*|❌|✅|✅|
|Advanced SQL tutorials *(Roadmap)*|❌|✅|✅|
|Smart auto-deletion and/or auto-deletion control *(Roadmap)*|❌|✅|✅|
|Channel mapping: one Joe can work with multiple Database Lab instances / multiple databases|❌|✅|✅|
|Share session *(Roadmap)*|❌|✅|✅|
|||||
|**Healthcheck (postgres-checkup)**||||
|Reports and recommendations|✅|✅|✅|
|Deep SQL performance analysis|✅|✅|✅|
|Centralized storage of all reports|❌|✅|✅|
|Performance and problems trend analysis *(Roadmap)*|❌|✅|✅|
|||||
|**BI**||
|Run long-running queries not affecting production health|✅|✅|✅|
|Named queries / query organizer|❌|✅|✅|
|||||
|**Security and compliance**||||
|Login via: Google, LinkedIn, GitHub, GitLab|✅|✅|✅|
|Invite team members|✅|✅|✅|
|Integration with project management systems (Jira, GitHub Issues, GitLab Issues, etc) *(Roadmap)*|❌|✅|✅|
|Automated sign-in using corporate email domain|❌|✅|✅|
|SSO (Okta, ActiveDirectory) *(Roadmap)*|❌|✅|✅|
|Audit log *(Roadmap)*|❌|✅|✅|
|Security dashboard *(Roadmap)*|❌|✅|✅|
|Roles / Team management / Personal tokens|❌|✅|✅|
|Quotas management|❌|✅|✅|
|||||
|**Support and deployment**||||
|Community support|✅|✅|✅|
|Standard support|❌|✅|✅|
|Premium support (24/7, 1 hour), trainings|❌|✅|✅|

See also: [Development roadmap](/docs/roadmap).
