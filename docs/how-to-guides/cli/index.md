---
title: How to work with Database Lab CLI
sidebar_label: Overview
slug: /how-to-guides/cli
---

## Guides
- [How to install and initialize Database Lab CLI](/docs/how-to-guides/cli/cli-install-init)
