import React, { useEffect } from "react";
import Layout from "@theme/Layout";

function CaseStudies() {
  useEffect(() => {
    // The `case-studies` directory is for better URLs,
    // redirecting to resources in case of manual manipulations with URLs.
    window.location.replace("/resources/");
  }),[];

  return (<div></div>);
}

export default CaseStudies;
